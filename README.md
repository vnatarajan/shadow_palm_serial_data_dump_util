## Shadow Plam Serial Data Dump Utility

## Interpreter 

    - Python 3

## Additional Packages 

    - python3 -m pip install pyserial

## Quick Run ( Linux / Mac )

    - ./run.sh

## Quick Run ( Windows )

#### Steps to run serial data dump utility: ( Windows )

	1. Go to Desktop
	2. Double Click "serial_dump.bat"

#### Steps to stop serial data dump utility: ( Windows )

    1. make sure your command prompt is focussed
    2. press ctrl + c
    3. In the following prompt, Feed N

        Batchvorgang abbrechen (J/N)? ->  N
    
    4. Dump file will be stored in desktop @ "C:\Users\isypj\Desktop\shadowpalm_serialdata_dumputil-master\src\dump.txt"
