'''
@Project Name   : Shadow Palm Serial Data Dump Utility
@Author         : Vignesh Natarajan
@Contact        : vnatarajan@uni-bielefeld.de
'''

import sys
import random
import time
import signal
#pip uninstall pyserial
#pip install pyserial
#pip3 install pyserial
#sudo pip3 install pyserial
#python2 -m pip install pyserial
import serial
import serial.tools.list_ports
from datetime import datetime

def get_sys_time():
    return str(datetime.now())

DEBUG=False
serial_handle = None
quit_util=False
out_file = "dump.txt"
file_handle = None

def sig_handler(sig, frame):
    global quit_util
    #print("   Users Input : Ctrl + C")
    quit_util = True

class SerialMonitor:
    def __init__(self):
        self.handle = None
        self.available = False
        self.connected = False
        self.dummy_value="  < Select Serial Port >  "
        self.serial_port_str = None
        self.baudrate = 9600

    def scan(self):
        global sport_list_str
        sport_list_raw = serial.tools.list_ports.comports()
        sport_list_str=[self.dummy_value]

        for element in sport_list_raw:
            sport_list_str.append(str(element.device))

        if len(sport_list_str) > 1:
            self.avaliable = True
        else:
            self.avaliable = False

        return sport_list_str

    def connect(self, serial_port_str, baud_rate):
        try:
            self.serial_port_str = serial_port_str
            self.baudrate = baud_rate
            self.handle = serial.Serial(self.serial_port_str, self.baudrate)
        except:
            print("error: serial connect -> "+serial_port_str)
            self.connected=False
            return

        self.connected=True
        try:
            self.handle.flushInput()
        except:
            print("error: serial flush -> "+self.serial_port_str)
            self.connected=False

    def close(self):
        if self.connected == True:
            self.handle.close()
            self.connected = False

    def read(self):
        serial_data = None

        if self.connected == True:
            try:
                serial_data = self.handle.readline()
            except:
                print("error: serial read -> "+self.serial_port_str)
                self.connected=False

        return serial_data

def Serial_Monitor_Routine():
    global serial_handle, file_handle, ctr
    global serial_port, baud_rate
    data_raw = None


    if serial_handle.connected == False:
        serial_handle.connect(serial_port, baud_rate)
        if serial_handle.connected == False:
            return

    data_raw = serial_handle.read()
    data_str = "[ "+ get_sys_time() + " ] " +str(data_raw)
    ctr+=1
    print("    Data Frames Dumped : "+str(ctr), end = "\r")
    file_handle.write(data_str)

def MAIN():
    global serial_handle, quit_util, file_handle, ctr
    ctr = 0
    print("")
    print("")
    print("    Press 'ctrl + c' to exit utility")
    print("")
    print("")
    signal.signal(signal.SIGINT, sig_handler)
    serial_handle = SerialMonitor()
    file_handle = open(out_file, "w")

    one_ms=0.001
    half_ms=0.0005
    while quit_util == False:
        Serial_Monitor_Routine()
        time.sleep(one_ms)

    file_handle.flush()
    file_handle.close()
    print("\n\n    Serial Dump is available @ dump.txt\n\n")

def ARG_CHECK():
    global serial_port, baud_rate
    serial_port = None
    baud_rate = None

    try:
        serial_port = sys.argv[1]
    except:
        pass

    try:
        baud_rate = sys.argv[2]
    except:
        pass


    if serial_port == None:
        print("error: feed serial port name as argument")
        exit(1)

    if baud_rate == None:
        print("error: feed baudrate name as argument")
        exit(1)

    print("Serial Port  : "+str(serial_port))
    print("Baud Rate    : "+str(baud_rate))

ARG_CHECK()
MAIN()

